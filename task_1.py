# Вивести у консоль перші 100 чисел Фібоначчі (див. у рекомендованих ресурсах)
# 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, ...

# 0 - current = 0
# 1 - next_num = 1
# 2 -

current = 0
next_num = 1

for _ in range(100):
    print(current)
    current, next_num = next_num, current + next_num


