# У реченні “Hello world” замінити всі літери “o” на “a”, а літери “l” на “e”.

text = "Hello world"

result_text = ""

for letter in text:
    if letter == "o":
        result_text += "a"
    elif letter == "l":
        result_text += "e"
    else:
        result_text += letter

print(result_text)
